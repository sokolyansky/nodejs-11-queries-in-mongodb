'use strict';

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const port = process.env.PORT || 3000;

const mongodb = require('mongodb');
const MongoClient = mongodb.MongoClient;
const url = 'mongodb://localhost:27017/ref';

let collection;

MongoClient.connect(url, (err, db) => {
  if (err) {
    console.log('Невозможно подключиться к базе данных MongoDB. Ошибка ', err)
  } else {
    console.log('Соединение установлено для ', url); // выводится 2 раза. Почему?

    collection = db.collection('phonebook');
  }
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.get('/', (req, res) => {

      collection.find().toArray(function (err, result) {
        if(err) {
          console.log(err);
        } else if(result.length) {
          // console.log(result);
          res.json(result);
        } else {
          console.log('Нет документов в базе данных.');
        }
      });
});

app.post('/', (req, res) => {
  if(!Object.keys(req.body).length) {
    //res.sendStatus(404);
    throw new Error('No data for insert!');
  }
  // const user = {phone: req.body.phone, name: req.body.name, surname: req.body.surname};
  collection.insert(req.body, (error, result) => {  // можно просто передать целый объект req.body
    if (error) {
      console.error('Не удалось внести данные в коллекцию. Ошибка: ', error);
      return;
    }

    res.send(`User ${req.body.name} ${req.body.surname} was created!`);
    //res.send(`User ${result.ops.name} ${result.ops.surname} was created!`);
  });

  // db.close();   // как закрывать соединение с БД
});

app.put('/', (req, res) => {
  if (!Object.keys(req.body).length) {
    // res.sendStatus(404);
    throw new Error('No data for update!');
  }

  const _id = new mongodb.ObjectID(req.body.id);
  delete req.body.id;

  collection.update({_id}, {$set: req.body}, (error, result) => {
    if (error) {
      console.error('Не удалось обновить данные в коллекции. Ошибка: ', error);
      return;
    }
    collection.findOne({_id}, (error, result) => {
      res.json(result);
      console.log(result);
    });

  })
});

app.delete('/', (req, res) => {  // {body} - деструктуризация в nodejs v5 не поддерживается
  if (!Object.keys(req.body).length) {
    throw new Error('No data for delete!')
  }

  const _id = new mongodb.ObjectID(req.body.id);
  collection.findOneAndDelete({_id}, (error, result) => {
    if (error) {
      console.log('не удалось удалить данные в коллекции!');
      return;
    }
    res.json(result);
  });
});

app.get('/search', (req, res) => {
  if (!Object.keys(req.query).length) {
    throw new Error('No data for searching!');
  }
  collection.find(req.query).toArray((error, result) => {
    if (error) {
      console.log('Не удалось найти данные, соответствующие запросу!');
      return;
    }
    res.json(result);
  });
});

app.use((err, req, res, next) => {
  if (err) {
    res.status(404).send(err.stack);
    // console.log(err); // после отправки ответа, код продолжает выполняться (выводится в консоль)
  }
});

app.listen(port, () => {
  console.log('HTTP server started on port %d', port);
});